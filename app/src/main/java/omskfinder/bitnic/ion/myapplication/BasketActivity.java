package omskfinder.bitnic.ion.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import omskfinder.bitnic.ion.myapplication.model.MBasket;
import omskfinder.bitnic.ion.myapplication.model.MProduct;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.utils.IAction;

import static omskfinder.bitnic.ion.myapplication.orm2.Configure.getSession;

public class BasketActivity extends AppCompatActivity {

    private ListView mListView;
    private ListAdapterForBasket adapterForBasket;
    private List<MBasket> mBaskets;
    private List<MProduct> mProducts;
    private TextView total;
    private Button btContinue, btClearBasket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
        mListView = (ListView) findViewById(R.id.list_basket);
        total = (TextView) findViewById(R.id.text_basket_total);

        btContinue = (Button) findViewById(R.id.bt_basket_continue);
        btClearBasket = (Button) findViewById(R.id.bt_clear_basket);


        findViewById(R.id.image_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(BasketActivity.this, CommitActivity.class));
            }
        });
        btClearBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UtilsCore.alertDialog(BasketActivity.this, getString(R.string.dff),
                        getString(R.string.dfg),
                        getString(R.string.rt4),
                        getString(R.string.tyy),
                        new IAction() {
                            @Override
                            public void action(Object o) {
                                getSession().deleteTable("basket");
                                mBaskets.clear();
                                adapterForBasket.notifyDataSetChanged();
                                UtilsCore.UpdateImage(BasketActivity.this);
                                refreshTotalBasket();
                                UtilsCore.vibration(BasketActivity.this);
                            }
                        });

            }
        });


        mBaskets = getSession().getList(MBasket.class, null);

        refreshList();
        UtilsCore.UpdateImage(this);
        refreshTotalBasket();

    }

    private void refreshList() {
        adapterForBasket = new ListAdapterForBasket(this, R.layout.item_basket, mBaskets, new IAction() {
            @Override
            public void action(Object o) {
                UtilsCore.UpdateImage(BasketActivity.this);
                refreshTotalBasket();
            }
        });
        mListView.setAdapter(adapterForBasket);
    }

    private void refreshTotalBasket() {
        double summ = 0;
        // List<MBasket> mBaskets1 = Configure.getSession().getList(MBasket.class, null);
        if (mBaskets.size() > 0) {
            if (mProducts == null) {
                mProducts = Configure.getSession().getList(MProduct.class, null);
            }

            for (MBasket mBasket : mBaskets) {
                for (MProduct mProduct : mProducts) {
                    if (mBasket.product_id.equals(mProduct.id)) {
                        summ = summ + mBasket.amount * mProduct.price;
                        break;
                    }
                }

            }

            total.setText(getString(R.string.заказнасумму) + String.valueOf(summ) + UtilsCore.getRub());
            btContinue.setVisibility(View.VISIBLE);
            btClearBasket.setVisibility(View.VISIBLE);
        } else {
            total.setText("");
            btClearBasket.setVisibility(View.GONE);
            btContinue.setVisibility(View.GONE);
        }
    }

    class ListAdapterForBasket extends ArrayAdapter<MBasket> {

        @NonNull
        private final Context context;
        private final int resource;
        @NonNull
        private final List<MBasket> objects;
        private IAction iAction;

        public ListAdapterForBasket(@NonNull Context context, @LayoutRes
                int resource, @NonNull List<MBasket> objects,
                                    IAction iAction) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.objects = objects;
            this.iAction = iAction;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            View mView = convertView;
            final MBasket p = getItem(position);
            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            TextView textSumma = mView.findViewById(R.id.text_summa);
            TextView nameproduct = mView.findViewById(R.id.basket_name);
            TextView textAmount = mView.findViewById(R.id.text_amount);
            TextView textPrice = mView.findViewById(R.id.text_price);
            TextView textMin = mView.findViewById(R.id.text_minimum);

            final List<MProduct> mProducts = getSession().getList(MProduct.class, " id = ?", p.product_id);
            if (mProducts.size() > 0) {
                nameproduct.setText(mProducts.get(0).name);
                double s = p.amount * mProducts.get(0).price;
                textSumma.setText(String.valueOf(s) + UtilsCore.getRub());
                textAmount.setText("Заказано: " + String.valueOf(p.amount) + " едениц.");
                textPrice.setText(String.valueOf(mProducts.get(0).price) + UtilsCore.getRub());
                mView.findViewById(R.id.basket_delete_product).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UtilsCore.alertDialog(BasketActivity.this, getString(R.string.dff),

                                getString(R.string.gggggggggrt),
                                getString(R.string.errrer),
                                getString(R.string.erer),
                                new IAction() {
                            @Override
                            public void action(Object o) {
                                if (Configure.getSession().delete(p) > 0) {
                                    objects.remove(p);
                                    ListAdapterForBasket.this.notifyDataSetChanged();
                                    iAction.action(null);
                                    UtilsCore.vibration(BasketActivity.this);
                                }
                            }
                        });

                    }
                });
                mView.findViewById(R.id.tb_basket_minus).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        p.amount = p.amount - 1 * mProducts.get(0).multiplicity;
                        if (p.amount <= 0) {
                            objects.remove(p);
                            Configure.getSession().delete(p);
                            ListAdapterForBasket.this.notifyDataSetChanged();
                            UtilsCore.vibration(getContext());
                        } else {
                            int i = Configure.getSession().update(p);
                            ListAdapterForBasket.this.notifyDataSetChanged();
                            UtilsCore.vibration(getContext());
                        }
                        iAction.action(null);
                    }
                });
                mView.findViewById(R.id.tb_basket_plus).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        p.amount = p.amount + 1 * mProducts.get(0).multiplicity;
                        ListAdapterForBasket.this.notifyDataSetChanged();
                        if (getSession().update(p) > 0) {
                            iAction.action(null);
                            UtilsCore.vibration(getContext());
                        }
                    }
                });
                textMin.setText("Минимум: " + String.valueOf(mProducts.get(0).multiplicity));
            } else {
                textSumma.setText("");
                nameproduct.setText(R.string.noneprod);
                nameproduct.setTextColor(Color.RED);
                textAmount.setText("");
                textPrice.setText("");
                textMin.setText("");
                mView.findViewById(R.id.basket_delete_product).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Configure.getSession().delete(p) > 0) {
                            objects.remove(p);
                            ListAdapterForBasket.this.notifyDataSetChanged();
                            iAction.action(null);
                        }
                    }
                });
            }

            return mView;
        }

    }
}
