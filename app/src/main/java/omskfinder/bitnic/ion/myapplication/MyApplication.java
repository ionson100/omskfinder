package omskfinder.bitnic.ion.myapplication;

import android.os.Environment;

import java.io.File;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.mindpipe.android.logging.log4j.LogConfigurator;


/**
 * Created by USER on 29.05.2018.
 */

public class MyApplication extends android.app.Application  {


    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainActivity.class);
    public static final String logfile = Environment.getExternalStorageDirectory().toString() + File.separator + "log1/log.txt";
    static {
        final LogConfigurator logConfigurator = new LogConfigurator();

        logConfigurator.setFileName(logfile);
        logConfigurator.setRootLevel(org.apache.log4j.Level.ALL);
        logConfigurator.setLevel("org.apache", org.apache.log4j.Level.ALL);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(1024 * 1024 * 5);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
        disableSslVerification();
    }


    // процедура отсылки ошибки на сервер
    private void handleUncaughtException(Thread thread, Throwable e) {

        StackTraceElement[] stackTraceElements = e.getStackTrace();
        StringBuilder builder = new StringBuilder();
        builder.append("Exception: ").append(e.getClass().getName()).append("\n")
                .append("Message: ").append(e.getMessage()).append("\nStacktrace:\n");


        for (StackTraceElement element : stackTraceElements) {
            builder.append("\t").append(element.toString()).append("\n");
        }
        log.trace("Hanler ",e);
        //Configure.getSession().insert(new MError(builder.toString()));







        System.exit(0);
    }

    @Override
    public void onCreate() {

        super.onCreate();



        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {

                handleUncaughtException(thread, e);

            }
        });

        ////////////////

    }

    @Override
    public void onTerminate() {

        //Configure.getSession().insert(new MError());
        super.onTerminate();
        log.info("onTerminate");
    }

    @Override
    public void onLowMemory() {

        //Configure.getSession().insert(new MError(""));
        super.onLowMemory();
        log.info("onLowMemory");
    }


    private static void disableSslVerification() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
           log.error(e);
        }
    }


}
