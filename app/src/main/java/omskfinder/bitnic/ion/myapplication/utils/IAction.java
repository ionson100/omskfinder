package omskfinder.bitnic.ion.myapplication.utils;



public interface IAction<T> {
    void action(T t);
}
