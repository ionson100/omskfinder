package omskfinder.bitnic.ion.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import omskfinder.bitnic.ion.myapplication.model.MRequisition;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;

public class CommitActivity extends AppCompatActivity {

    private EditText editName,fioTel;
    String tel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_commit);


        editName= (EditText) findViewById(R.id.edit_name);
        fioTel= (EditText) findViewById(R.id.edit_telephone);
        findViewById(R.id.bt_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name=editName.getText().toString().trim();
                if(name.length()==0){
                    editName.setError(getString(R.string.adad));
                    return;
                }
                if(tel.trim().length()<11){
                    fioTel.setError(getString(R.string.adad));
                    return;
                }
                List<MRequisition> list= Configure.getSession().getList(MRequisition.class,null);
                if(list.size()==0){
                    MRequisition f=new MRequisition();
                    f.name=name;
                    f.telephone=tel;
                    Configure.getSession().insert(f);
                }else {
                    MRequisition f=list.get(0);
                    f.name=name;
                    f.telephone=tel;
                    Configure.getSession().update(f);
                }
            }
        });
        fioTel.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - fioTel.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");

                if (!editedFlag) {

                    if (phone.length() >= 6 && !backspacingFlag) {

                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        fioTel.setText(ans);
                        fioTel.setSelection(fioTel.getText().length() - cursorComplement);
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        fioTel.setText(ans);
                        fioTel.setSelection(fioTel.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });

        fioTel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String dd = s.toString().replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
                if (dd.length() > 5)
                    tel = "8" + dd;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        List<MRequisition> list= Configure.getSession().getList(MRequisition.class,null);
        if(list.size()>0){
            MRequisition f=list.get(0);
            editName.setText(f.name);
            fioTel.setText(f.telephone.substring(1));

        }

        ImageButton imageButton= (ImageButton) findViewById(R.id.image_basket);//
        imageButton .setVisibility(View.INVISIBLE);
        TextView textView= (TextView) findViewById(R.id.text_amount_basket);
        textView.setVisibility(View.INVISIBLE);

        findViewById(R.id.image_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommitActivity.this.finish();
            }
        });
        //fioTel.setText(mPoint.telephone_fio);image_back

    }

}
