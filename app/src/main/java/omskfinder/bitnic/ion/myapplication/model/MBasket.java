package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;



@Table("basket")
public class MBasket {

    @PrimaryKey("_id")
    public int _id;

    @Column("product_id")
    public String product_id;

    @Column("amount")
    public double amount;


}
