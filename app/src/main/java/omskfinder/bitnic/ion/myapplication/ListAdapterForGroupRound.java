package omskfinder.bitnic.ion.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import omskfinder.bitnic.ion.myapplication.model.MGroup;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;

public class ListAdapterForGroupRound extends ArrayAdapter<List<MGroup>> {

    @NonNull
    private final Context context;
    private final int resource;
    @NonNull
    private final List<List<MGroup>> objects;

    public ListAdapterForGroupRound(@NonNull Context context, @LayoutRes int resource, @NonNull List<List<MGroup>> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        final List<MGroup> p = getItem(position);
        mView = LayoutInflater.from(getContext()).inflate(resource, null);


        ImageButton bt1 = mView.findViewById(R.id.bt_1);
        UtilsCore.imagerPizdaticus(bt1, p.get(0));
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(p.get(0));
            }
        });


        ImageButton bt2 = mView.findViewById(R.id.bt_2);
        if(p.size()>=2){
            UtilsCore.imagerPizdaticus(bt2, p.get(1));
            bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click(p.get(1));
                }
            });
        }



        ImageButton bt3 = mView.findViewById(R.id.bt_3);


        if(p.size()>=3){
            UtilsCore.imagerPizdaticus(bt3, p.get(2));
            bt3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click(p.get(2));
                }
            });
        }

        mView.setTag(p);
        return mView;
    }

    private  void click(MGroup mGroup){

        Object d = Configure.getSession().executeScalar("select count(*) from product_data where group_data = ?", mGroup.id);
        if (String.valueOf(d).equals("0")) {
            Toast.makeText(getContext(), R.string.nosearchproduct, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getContext(), ProductActivity.class);
            intent.putExtra("key", mGroup.id);
            getContext().startActivity(intent);
        }
    }
}
