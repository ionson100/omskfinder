package omskfinder.bitnic.ion.myapplication.model;

import java.util.ArrayList;
import java.util.List;



public class ListerGroup {
    public static final List<Class> classes=new ArrayList<>();
    static {
        classes.add(MProduct.class);
        classes.add(MGroup.class);
        classes.add(MBasket.class);
        classes.add(MHuylo.class);
        classes.add(MRequisition.class);
        classes.add(MMap.class);
        classes.add(MPoint.class);
    }
}
