package omskfinder.bitnic.ion.myapplication;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import omskfinder.bitnic.ion.myapplication.model.MHuylo;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;

public class ContractActivity extends AppCompatActivity {

    Button bt_save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract);
        findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });

        bt_save= (Button) findViewById(R.id.bt_save);
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Configure.getSession().insert(new MHuylo())>0){
                    finish();
                }

            }
        });
        AssetManager am = this.getAssets();
        InputStream is = null;
        try {
            is = am.open("note");
        } catch (IOException ignored) {
            return;
        }

        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
            r.close();
        } catch (IOException ignored) {
            return;
        }
        TextView textView= (TextView) findViewById(R.id.text_note);
        textView.setText(total.toString());
        CheckBox checkBox= (CheckBox) findViewById(R.id.checkBox);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    bt_save.setEnabled(true);
                }else {
                    bt_save.setEnabled(false);
                }
            }
        });
    }
}
