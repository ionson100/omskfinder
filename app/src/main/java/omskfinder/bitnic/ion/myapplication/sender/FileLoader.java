package omskfinder.bitnic.ion.myapplication.sender;

import android.content.Context;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import omskfinder.bitnic.ion.myapplication.UtilsCore;
import omskfinder.bitnic.ion.myapplication.model.MGroup;
import omskfinder.bitnic.ion.myapplication.model.MProduct;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.utils.Downloader;

/**
 * Created by USER on 30.05.2018.
 */

public class FileLoader implements Runnable {
    private static ExecutorService service = Executors.newFixedThreadPool(10);
    private Context context;

    public FileLoader(Context context) {

        this.context = context;
    }

    @Override
    public void run() {
        List<MGroup> mGroups = Configure.getSession().getList(MGroup.class, null);
        List<MProduct> mProducts = Configure.getSession().getList(MProduct.class, null);
        Set<String> linkList = new HashSet<>();
        for (MGroup mGroup : mGroups) {
            if(mGroup.photo==null) continue;
            linkList.add(mGroup.photo.trim());
        }
        for (MProduct mProduct : mProducts) {
            //linkList.add(mProduct.preview.trim());
            if(mProduct.photo==null) continue;
            linkList.add(mProduct.photo);

        }

        Set<String> sl=new HashSet<>();
        {
            File file=new File(UtilsCore.getPromotionDirectory(context));
            File[] files=file.listFiles();
            if(files!=null){
                for (File ff : files) {
                    if(ff.getPath().contains(Downloader.HLAM)){
                        ff.delete();
                    }else {
                        sl.add(ff.getPath().trim());
                    }
                }
            }
        }



        for (Object file : linkList.toArray()) {
            if (file == null) continue;
            String s = (String) file;

            boolean out=false;
            for (String s1 : sl) {
                int  ss1=getFileName(s1).hashCode();
                int  ss2=getFileName(s).hashCode();
                if(ss1==ss2){
                    out=true;
                   break;
                }
            }
            if(out) continue;

            try{
                s = s.substring(s.lastIndexOf("/"));
            }catch (Exception ex){
                continue;
            }



            String path = UtilsCore.getPromotionDirectory(context) + s;

            service.submit(new Downloader().setUrl("https://138.197.161.95" + file).setPath(path));//.execute ();

        }


    }
    public static String getFileName(String path){
        return path.substring(path.lastIndexOf("/")+1);
    }
}
