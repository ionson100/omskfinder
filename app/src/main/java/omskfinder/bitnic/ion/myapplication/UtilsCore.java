package omskfinder.bitnic.ion.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.List;

import omskfinder.bitnic.ion.myapplication.model.MBasket;
import omskfinder.bitnic.ion.myapplication.model.MGroup;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.utils.IAction;

/**
 * Created by USER on 29.05.2018.
 */

public class UtilsCore {
    public static final String BASE_NAME = "omskfinder.sqlite";

    public static String getPromotionDirectory(Context context) {
        return context.getApplicationInfo().dataDir + "/omskfinder";
    }

    public static String readTextFile(String path) throws Exception {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();
        }
    }

    public static void rewriteFile(String fileName, String s) throws Exception {
        File myFoo = new File(fileName);
        try (FileOutputStream fooStream = new FileOutputStream(myFoo, false)) { // true to append
            byte[] myBytes = s.getBytes();
            fooStream.write(myBytes);
        }


    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static synchronized void UpdateImage(Activity view) {
        ImageButton imageButton = view.findViewById(R.id.image_basket);
        TextView textView = view.findViewById(R.id.text_amount_basket);
        List<MBasket> mBaskets = Configure.getSession().getList(MBasket.class, null);
        if (mBaskets.size() == 0) {
            textView.setText("");
            imageButton.setImageResource(R.drawable.button_basket);
        } else {
            int sum = 0;
            for (MBasket mBasket : mBaskets) {
                sum = (int) (sum + mBasket.amount);
            }
            imageButton.setImageResource(R.drawable.button_number_basket);
            textView.setText(String.valueOf(sum));
        }
    }

    public static synchronized void UpdateImage(Activity view, List<MBasket> mBaskets) {
        ImageButton imageButton = view.findViewById(R.id.image_basket);
        TextView textView = view.findViewById(R.id.text_amount_basket);

        if (mBaskets.size() == 0) {
            textView.setText("");
            imageButton.setImageResource(R.drawable.button_basket);
        } else {
            int sum = 0;
            for (MBasket mBasket : mBaskets) {
                sum = (int) (sum + mBasket.amount);
            }
            imageButton.setImageResource(R.drawable.button_number_basket);
            textView.setText(String.valueOf(sum));
        }
    }


    public static String getRub() {
        return " " + Html.fromHtml("&#x20bd");
    }


    public static void vibration(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(300);
        }
    }

    public static void alertDialog(Activity activity, String btCancel, String btOk, String title, String body, final IAction iAction) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        View view = activity.getLayoutInflater()
                .inflate(R.layout.dialog_alert, null);
        ((TextView) view.findViewById(R.id.text_title)).setText(title);
        ((TextView) view.findViewById(R.id.text_body)).setText(body);
        Button bt_cancel = view.findViewById(R.id.bt_cancel);
        Button bt_ok = view.findViewById(R.id.bt_ok);
        bt_cancel.setText(btCancel);
        bt_ok.setText(btOk);
        adb.setView(view);
        final AlertDialog alertDialog = adb.create();
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAction.action(null);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static void  imagerPizdaticus(ImageButton imageButton, MGroup mGroup){
        String s=mGroup.id;
        switch (s){
            case "00000012518":{
                imageButton.setImageResource(R.drawable.button_group_1);//чай
                break;
            }
            case "00000012519":{
                imageButton.setImageResource(R.drawable.button_group_2);//кофе
                break;
            }
            case "00000012678":{
                imageButton.setImageResource(R.drawable.button_group_3);//табак трубочный
                break;
            }
            case "00000013443":{
                imageButton.setImageResource(R.drawable.button_group_4);//напитки
                break;
            }
            case "00000012624":{
                imageButton.setImageResource(R.drawable.button_group_5);//абак и уголь для кальяна
                break;
            }
            case "00000012771":{
                imageButton.setImageResource(R.drawable.button_group_6);// шоколад
                break;
            }
            case "00000012580":{
                imageButton.setImageResource(R.drawable.button_group_7);//зажигалки
                break;
            }
            case "00000012629":{
                imageButton.setImageResource(R.drawable.button_group_8);//Аксессуары
            }
            case "00000012586":{
                imageButton.setImageResource(R.drawable.button_group_9);//трубки
                break;
            }
            case "00000012578":{
                imageButton.setImageResource(R.drawable.button_group_10);//Сигареты ЮТФ
                break;
            }
            case "00000013572":{
                imageButton.setImageResource(R.drawable.button_group_11);//Гигиена
                break;
            }
            case "00000012585":{
                imageButton.setImageResource(R.drawable.button_group_12);//Сигариллы ЮТФ
                break;
            }
            default:{
                throw new RuntimeException("идиот добалил группу");
            }


        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}
