package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;

/**
 * Created by USER on 04.06.2018.
 */
@Table("mapgeo")
public class MMap {
    @PrimaryKey("_id")
    public int _id;
    @Column("lat")
    public double lat;
    @Column("lon")
    public double lon;
}
