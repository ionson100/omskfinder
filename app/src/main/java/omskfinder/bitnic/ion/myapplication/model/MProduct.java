package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;

/**
 * Created by USER on 29.05.2018.
 */

@Table("product_data")
public class MProduct {

    @PrimaryKey("_id")
    public int _id;

    @Column("id")
    public String id;

    @Column("group_data")
    public String group;

    @Column("name")
    public String name;

    @Column("photo")
    public String photo;

    @Column("price")
    public double price;

    @Column("preview")
    public String preview;

    @Column("description")
    public String description;

    @Column("multiplier")
    public int multiplicity;
}
