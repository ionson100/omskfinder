package omskfinder.bitnic.ion.myapplication.sender;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import omskfinder.bitnic.ion.myapplication.MainActivity;
import omskfinder.bitnic.ion.myapplication.model.MGroup;
import omskfinder.bitnic.ion.myapplication.model.MPoint;
import omskfinder.bitnic.ion.myapplication.model.MProduct;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.orm2.ISession;
import omskfinder.bitnic.ion.myapplication.utils.IAction;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;


public class SenderLoadData {


    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainActivity.class);
    Exception exception;
    private Context activity;
    private IAction iAction;

    public void loadData(Context activity, IAction iAction) {
        this.activity = activity;

        this.iAction = iAction;
//        if(UtilsCore.isOnline(activity)){
            new SenderWorkerTack().executeOnExecutor(THREAD_POOL_EXECUTOR, null);
//        }else {
//            Toast.makeText(activity, R.string.sadasd, Toast.LENGTH_SHORT).show();
//        }

    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, Void> {

        public class NullHostNameVerifier implements HostnameVerifier {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                Log.i("RestUtilImpl", "Approving certificate for " + hostname);
                return true;
            }

        }

        @Override
        protected Void doInBackground(Void... voids) {

            InputStream is = null;
            BufferedReader reader = null;
            HttpsURLConnection conn = null;
            try {

                HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                String uu = String.format("https://%s/app_main", "138.197.161.95");
                URL u = new URL(uu);
                conn = (HttpsURLConnection) u.openConnection();
                conn.setInstanceFollowRedirects(false);
                conn.setReadTimeout(15000 /*milliseconds*/);
                conn.setConnectTimeout(20000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.connect();
                int status = conn.getResponseCode();

                if (status == 200) {

                    is = conn.getInputStream();
                    reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }

                    LoadData c = new Gson().fromJson(stringBuilder.toString(), LoadData.class);
                    ISession iSession = Configure.getSession();

                    iSession.deleteTable("`group`");
                    iSession.deleteTable("product_data");

                    Configure.bulk(MProduct.class,c.products,iSession);
                    //List<MProduct> mProducts=Configure.getSession().getList(MProduct.class,null);
                    Configure.bulk(MGroup.class,c.groups,iSession);

                    Configure.bulk(MPoint.class,c.points,iSession);
                    {
                        MPoint mPoint=new MPoint();
                        mPoint.address="jfkdf fjdksjfkj fd";
                        mPoint.lat=56.822;
                        mPoint.lon=60.6323;
                        iSession.insert(mPoint);
                    }
                    {
                        MPoint mPoint=new MPoint();
                        mPoint.address="jfkdf fjdksjfkj fd";
                        mPoint.lat=56.816;
                        mPoint.lon=60.623;
                        iSession.insert(mPoint);
                    }


                } else {
                    throw new RuntimeException(uu + " ответ - " + String.valueOf(status));
                }

            } catch (Exception ex) {

                exception = ex;
                log.error(ex);

            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                try {
                    if (is != null) {
                        is.close();
                    }

                    if (reader != null) {
                        reader.close();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new Thread(new FileLoader(activity)).start();
            if (iAction != null) {
                iAction.action(exception);
            }
        }
    }
}

class NullX509TrustManager implements X509TrustManager {
    /**
     * Does nothing.
     *
     * @param chain
     *            certificate chain
     * @param authType
     *            authentication type
     */


    /**
     * Does nothing.
     *
     * @param chain    certificate chain
     * @param authType authentication type
     */
    @Override
    public void checkServerTrusted(final X509Certificate[] chain,
                                   final String authType) throws CertificateException {
        // Does nothing
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

    }

    /**
     * Gets a list of accepted issuers.
     *
     * @return empty array
     */
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
        // Does nothing
    }
}

class LoadData {
    List<MGroup> groups = new ArrayList<>();

    List<MProduct> products = new ArrayList<>();

    List<MPoint> points=new ArrayList<>();
}
