package omskfinder.bitnic.ion.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import omskfinder.bitnic.ion.myapplication.model.MGroup;
import omskfinder.bitnic.ion.myapplication.model.MHuylo;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;

import static omskfinder.bitnic.ion.myapplication.orm2.Configure.getSession;


public class MainActivity extends AppCompatActivity {

    private static boolean isShowList=false;
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainActivity.class);

    private ListView list_group;
    private  List<MGroup> mGroups;
    private ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 23) {


            String[] INITIAL_PERMS = {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.CHANGE_WIFI_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            };
            requestPermissions(INITIAL_PERMS, 1327);
        }
        //////////////////////////////////////// проверка на совершеннолетие

        List<MHuylo> mHuylos = Configure.getSession().getList(MHuylo.class, null);
        if (mHuylos.size() == 0) {
            startActivity(new Intent(MainActivity.this, ContractActivity.class));
        }


        /////////////////////////////////////////////////


        imageButton= (ImageButton) findViewById(R.id.image_type_group);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowList=!isShowList;
                imagePainter();
            }
        });
        list_group = (ListView) findViewById(R.id.list_group);

         mGroups = getSession().getList(MGroup.class, null);
        Collections.sort(mGroups, new Comparator<MGroup>() {
            @Override
            public int compare(MGroup o1, MGroup o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        imagePainter();



        findViewById(R.id.image_basket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, BasketActivity.class));
            }
        });
        UtilsCore.UpdateImage(this);
        findViewById(R.id.image_back).setVisibility(View.GONE);
        findViewById(R.id.button_type).setVisibility(View.VISIBLE);
    }

    void  imagePainter(){
        if(isShowList){
            imageButton.setImageResource(R.drawable.button_select_type_icon);
            ListAdapterForGroup groupAdapter = new ListAdapterForGroup(this, R.layout.item_group, mGroups);
            list_group.setAdapter(groupAdapter);
            list_group.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MGroup mGroup = (MGroup) view.getTag();
                    Object d = Configure.getSession().executeScalar("select count(*) from product_data where group_data = ?", mGroup.id);
                    if (String.valueOf(d).equals("0")) {
                        Toast.makeText(MainActivity.this, R.string.nosearchproduct, Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                        intent.putExtra("key", mGroup.id);
                        startActivity(intent);
                    }
                }
            });
        }else {
            imageButton.setImageResource(R.drawable.button_select_type_list);
            List<List<MGroup>>  lists=Configure.partition(mGroups,3);
            ListAdapterForGroupRound round=new ListAdapterForGroupRound(this,R.layout.item_round_group,lists);
            list_group.setAdapter(round);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        UtilsCore.UpdateImage(this);
    }

    static class ListAdapterForGroup extends ArrayAdapter<MGroup> {

        private Context context;
        private int resource;
        private List<MGroup> objects;

        public ListAdapterForGroup(@NonNull Context context, @LayoutRes int resource, @NonNull List<MGroup> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.objects = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;
            final MGroup p = getItem(position);
            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            TextView textView = mView.findViewById(R.id.text_group_name);
            textView.setText(p.name);
            mView.setTag(p);
            return mView;
        }
    }


}
