package omskfinder.bitnic.ion.myapplication.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by USER on 29.05.2018.
 */

public class Downloader implements Runnable {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Downloader.class);
    public final static String HLAM = ".nocomplete";
    private String urlCore, path;

    @Override
    public void run() {
        doInBackground();
    }

    protected void doInBackground() {

        int count;
        try {
            URL url = new URL(urlCore);
            HttpsURLConnection conection = (HttpsURLConnection) url.openConnection();
            conection.setInstanceFollowRedirects(false);
            conection.setReadTimeout(15000 /*milliseconds*/);
            conection.setConnectTimeout(20000 /* milliseconds */);
            conection.setRequestMethod("GET");
            conection.connect();
            int idd=conection.getResponseCode();

            int lenghtOfFile = conection.getContentLength() + 100;
            InputStream input = conection.getInputStream();
            OutputStream output = new FileOutputStream(path+HLAM);
            byte data[] = new byte[lenghtOfFile];
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            output.close();
            input.close();
            File file = new File(path);
            File file1 = new File(path+HLAM);
            file1.renameTo(file);
        } catch (Exception e) {
            log.error(e);

        }

    }


    public Downloader setUrl(String urlCore) {
        this.urlCore = urlCore.trim();
        return this;
    }

    public Downloader setPath(String path) {
        this.path = path.trim();
        return this;
    }
}
