package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;



@Table("`group`")
public class MGroup {

    @PrimaryKey("_id")
    public int _id;
    @Column("id")
    public String id;
    @Column("name")
    public String name;
    @Column("photo")
    public String photo;

}
