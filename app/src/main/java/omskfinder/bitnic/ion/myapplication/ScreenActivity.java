package omskfinder.bitnic.ion.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.File;

import omskfinder.bitnic.ion.myapplication.model.ListerGroup;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.sender.SenderLoadData;
import omskfinder.bitnic.ion.myapplication.utils.IAction;


public class ScreenActivity extends AppCompatActivity {
    public static String contractFile = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);
        contractFile = getApplicationInfo().dataDir + "/contract";
        new Configure(getApplicationInfo().dataDir + "/" + UtilsCore.BASE_NAME, getApplicationContext(), ListerGroup.classes);
        new SenderLoadData().loadData(getApplicationContext(), new IAction() {
            @Override
            public void action(Object o) {
                if (o != null) {
                    Toast.makeText(ScreenActivity.this, ((Exception) o).getMessage(), Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(ScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        String dd=UtilsCore.getPromotionDirectory(this);
        File fs = new File(dd);
        if (!fs.exists()) {
            boolean d=  fs.mkdirs();
        }

    }

}
