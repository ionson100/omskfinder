package omskfinder.bitnic.ion.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import omskfinder.bitnic.ion.myapplication.model.MBasket;
import omskfinder.bitnic.ion.myapplication.model.MProduct;
import omskfinder.bitnic.ion.myapplication.orm2.Configure;
import omskfinder.bitnic.ion.myapplication.sender.FileLoader;

public class ProductActivity extends AppCompatActivity {


    Set<String> productsBasketId = new HashSet<>();


    private ListAdapterForProduct adapterForProduct;
    private List<MProduct> mProducts;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Intent intent = getIntent();
        String value = intent.getStringExtra("key");
        mProducts = Configure.getSession().getList(MProduct.class, " group_data =? order by name", value);
        mListView = (ListView) findViewById(R.id.list_product);
        refreshList();


        findViewById(R.id.image_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProductActivity.this.finish();
            }
        });

        findViewById(R.id.image_basket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ProductActivity.this,BasketActivity.class));
            }
        });

        refreshBasket();



    }

    private void refreshList() {
        adapterForProduct = new ListAdapterForProduct(this, R.layout.item_product, mProducts);
        mListView.setAdapter(adapterForProduct);
    }

    @Override
    protected void onRestart() {
            super.onRestart();
        refreshBasket();
        refreshList();
        UtilsCore.UpdateImage(this);
    }

    private void refreshBasket() {
        List<MBasket> mBaskets = Configure.getSession().getList(MBasket.class, null);
        productsBasketId.clear();
        for (MBasket mBasket : mBaskets) {
            productsBasketId.add(mBasket.product_id);
        }
        UtilsCore.UpdateImage(this,mBaskets);
    }

    class ListAdapterForProduct extends ArrayAdapter<MProduct> {

        Set<String> stringsImage = new HashSet<>();
        private Context context;
        private int resource;
        private List<MProduct> objects;

        public ListAdapterForProduct(@NonNull Context context, @LayoutRes int resource, @NonNull List<MProduct> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.objects = objects;
            File file = new File(UtilsCore.getPromotionDirectory(ProductActivity.this));
            if (file.exists()) {
                File[] files = file.listFiles();
                if (files != null) {
                    for (File file1 : files) {
                        String f = FileLoader.getFileName(file1.getPath());
                        stringsImage.add(f.trim());
                    }

                }
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;
            final MProduct p = getItem(position);
            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            TextView textName = mView.findViewById(R.id.text_name_product);
            textName.setText(p.name);

            final LinearLayout layout = mView.findViewById(R.id.panel_description);

            final ImageButton imageButton= mView.findViewById(R.id.image_description);
           imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clicable(layout, imageButton);

                }
            });
            mView.findViewById(R.id.text_description).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clicable(layout,imageButton);
                }
            });
            if (p.description != null && p.description.length() > 0) {
                mView.findViewById(R.id.panel_button_description).setVisibility(View.VISIBLE);
            }
            TextView t1 = mView.findViewById(R.id.text_multipier);

            t1.setText(String.valueOf(p.multiplicity));

            TextView t2 = mView.findViewById(R.id.text_price);
            t2.setText(String.valueOf(p.price) + UtilsCore.getRub());

            if (p.photo != null && p.photo.length() > 0) {
                String f = FileLoader.getFileName(p.photo).trim();
                if (stringsImage.contains(f)) {
                    ImageView imageView = mView.findViewById(R.id.inmage_head);
                    String ff = UtilsCore.getPromotionDirectory(ProductActivity.this) + "/" + f;
                    imageView.setImageDrawable(Drawable.createFromPath(ff));
                }
            }
            final ImageButton image_minus = mView.findViewById(R.id.image_minus);
            mView.findViewById(R.id.image_plus).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<MBasket> list = Configure.getSession().getList(MBasket.class, " product_id = ?", p.id);
                    if (list.size() == 0) {
                        MBasket mBasket = new MBasket();
                        mBasket.amount = p.multiplicity;
                        mBasket.product_id = p.id;
                        if (Configure.getSession().insert(mBasket) > 0) {
                            Toast.makeText(context, "Продукт добавлен в корзину", Toast.LENGTH_SHORT).show();
                            image_minus.setVisibility(View.VISIBLE);
                            UtilsCore.vibration(getContext());
                        }
                    } else {
                        double d = list.get(0).amount;
                        list.get(0).amount = d + p.multiplicity;
                        if (Configure.getSession().update(list.get(0)) > 0) {
                            Toast.makeText(context, "Продукт добавлен в корзину", Toast.LENGTH_SHORT).show();
                            image_minus.setVisibility(View.VISIBLE);
                            UtilsCore.vibration(getContext());
                        }

                    }

                    refreshBasket();
                    UtilsCore.vibration(getContext());
                }
            });

            image_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<MBasket> list = Configure.getSession().getList(MBasket.class, " product_id = ?", p.id);
                    if (list.size() == 0) {
                    } else {
                        double d = list.get(0).amount - p.multiplicity;
                        list.get(0).amount = d;
                        if (d == 0 || d < 0) {
                            if (Configure.getSession().delete(list.get(0)) > 0) {
                                image_minus.setVisibility(View.INVISIBLE);
                                Toast.makeText(context, R.string.deleteprod, Toast.LENGTH_SHORT).show();
                                UtilsCore.vibration(getContext());
                            }
                        } else {
                            if (Configure.getSession().update(list.get(0)) > 0) {
                                Toast.makeText(context, R.string.deleteprod, Toast.LENGTH_SHORT).show();
                                UtilsCore.vibration(getContext());
                            }
                        }
                        refreshBasket();



                    }
                }
            });

            if (productsBasketId.contains(p.id)) {
                mView.findViewById(R.id.image_minus).setVisibility(View.VISIBLE);
            }
            mView.findViewById(R.id.image_map).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context,MapsActivity.class));
                }
            });




            return mView;
        }

        private void clicable(LinearLayout layout,ImageButton  imageButton) {
            if (layout.getVisibility() == View.VISIBLE) {
                layout.setVisibility(View.GONE);
                imageButton.setImageResource(R.drawable.open);
            } else {
                layout.setVisibility(View.VISIBLE);
                imageButton.setImageResource(R.drawable.hide);
            }
        }
    }
}
