package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;

/**
 * Created by USER on 04.06.2018.
 */

@Table("requisition")
public class MRequisition {

    @PrimaryKey("_id")
    public int _id;

    @Column("name")
    public String name;
    @Column("telephone")
    public String telephone;
}
