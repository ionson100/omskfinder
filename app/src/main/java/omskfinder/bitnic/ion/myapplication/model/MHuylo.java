package omskfinder.bitnic.ion.myapplication.model;

import omskfinder.bitnic.ion.myapplication.orm2.Column;
import omskfinder.bitnic.ion.myapplication.orm2.PrimaryKey;
import omskfinder.bitnic.ion.myapplication.orm2.Table;

/**
 * Created by USER on 04.06.2018.
 */
@Table("huylo")
public class MHuylo {

    @PrimaryKey("_id")
    public int _id;

    @Column("hh")
    public int hh;
}
